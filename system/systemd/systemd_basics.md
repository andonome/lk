---
title: "systemd"
tags: [ "systemd" ]
---
```bash
systemctl list-units
```

```bash
sudo systemctl status mpd
```

```bash
sudo systemctl daemon-reload
```

```bash
sudo systemctl taskd.service start
```

```bash
sudo systemctl status taskd.service
```

# Startup

```bash
sudo systemd-analyze
```

```bash
sudo systemd-analyze blame
```

