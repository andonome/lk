---
title: "fail2ban"
tags: [ "networking" ]
requires: [ "ssh" ]
---
# SSH Daemon Jail

```bash
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.d/ssh.local
```

```
[sshd]
enabled = true

ignoreip = 127.0.0.1/8 ::1,192.168.0.0/16 ::1

```

```bash
sudo systemctl restart fail2ban
```

```bash
sudo fail2ban-client status
```

```bash
sudo fail2ban-client status sshd
```



