---
title: "monitor"
tags: [ "hardware" ]
---
See screen size

```sh
xrandr -q
```

Automatically configure:

```sh
xrandr --auto
```

