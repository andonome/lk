output: all

.PHONY: example
example: html/foot.html html/head.html
	mkdir -p articles/
	fortune > articles/fort_1.md
	fortune > articles/fort_2.md

HTML = $(patsubst articles/%.md,public/%.html,$(wildcard articles/*.md))

$(HTML): public/ articles/ $(wildcard html/*)

html/head.html:
	@mkdir $(@D)
	echo '<head> Something about CSS probably </head>' > $@
	echo '<body>' >> $@

html/foot.html: html/head.html
	echo '</body>' >> $@

public/%.html : articles/%.md
	cat html/head.html > $@
	lowdown $< >> $@
	cat html/foot.html >> $@

.PHONY: all
all : $(HTML)

articles/:
	mkdir $@

public/:
	mkdir $@

clean :
	rm -rf public
