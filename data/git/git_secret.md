---
title: "git-secret"
tags: [ "data", "git" ]
---

This utility is largely useless, as it can only identify people by their email.
So if someone has multiple GPG keys associated with one email, the tool will not work.

A broken tool is better than a tool which will break soon.
