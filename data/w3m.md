---
title: "w3m"
tags: [ "browsers" ]
---
Open a search tab:

```bash
w3m ddg.gg
```

<Tab> then enter to start typing.

| Key      | Thing                |
|:---------|:---------------------|
|  H       | help                 |
| Tab      | switch fields        |
| o        | options              |
| B        | back                 |
| T        | new tab              |
| { / }    | switch tabs          |

