---
title: "gpg with vim"
tags: [ "vim", "data", "GPG" ]
requires: [ "GPG Basics", "vim basics" ]
---

The `vim-gnupg` plugin lets vim edit gpg-encrypted files as if they were unencrypted.

It's probably in your package manager.
If not, you'll need to endure the faff of following the [instructions](http://www.vim.org/scripts/script.php?script_id=3645).

