---
title: "links"
tags: [ "basics", "links" ]
---

There are two types:

- [Soft links](soft_links.md)
- [Hard links](hard_links.md)
