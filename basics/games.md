---
title: "bash games"
tags: [ "games" ]
---

Games are a great way to learn bash.

- `mapscii.me` is an interactive terminal map.
    1. Install telnet.
    1. `telnet mapscii.me`
- [Over the Wire](https://overthewire.org/wargames) teaches bash with small challenging you can do over `ssh`.
