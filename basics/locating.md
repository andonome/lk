---
title: "locating"
tags: [ "basics" ]
---
# Type

`type` shows what kind of thing you're running, be it an alias, function, or binary program.

```sh
type cd
type ls
type -P ls
type -a cat
```

# Whereis the Program

Where is `grep` and all its configuration files?

```sh
whereis grep
```

Which one of these is the binary file which you actually use?

```sh
which grep
```

# More

- [Search instantly with `plocate`](data/search_system.md)

