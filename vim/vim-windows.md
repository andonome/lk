---
title: "vim windows"
tags: [ "vim" ]
---

| Command | Keys |
|:-----|:----:|
| split window | C-w s |
| split window vertically | C-w v |
| close window | C-q |
| change window | C-w w |
| rotate windows | C-w r |
| split open new file | :sf path/file |

