---
title: "vim basics"
tags: [ "vim", "basic" ]
---

1. Insert text by pressing `i`.
1. Stop inserting text by pressing `Ctrl+[`.
1. Exit with `ZZ`.
1. Congratulations, you now know `vim`.

## Extras

- [Navigation](navigate.md)
- [Completion](vim-completion.md)
- [Search](vim-search.md)
- [Window Splits](vim-windows.md)
