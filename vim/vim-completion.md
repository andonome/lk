---
title: "vim completion"
tags: [ "vim" ]
---

Complete the word by searching for the *n*ext similar word:

> C-n

Complete the word by searching for a *p*revious similar word:

> C-p

Complete the full line:

> C-x C-l
