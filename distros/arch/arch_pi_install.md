---
title: "Arch on a Raspberry Pi 4"
tags: [ "distros", "raspberry pi", "rpi" ]
---

The [Official Instructions](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4) for a Raspberry pi 4 do not allow for working sound from the headphone jack, unless you use the aarch64 Installation.

