---
title: "extrace"
tags: [ "void" ]
---
Monitor all processes:

```sh
extrace
```

Monitor one process:

```sh
extrace ls
```

Monitor a script:

```sh
./script.sh | extrace
```

